/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.dataEngines;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import lk.blackout.utils.BlackOutConstants;
import lk.blackout.dataEnhancer.TokenEnhancer;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lk
 */
public class DBPedia {

    final Logger logger = LoggerFactory.getLogger(DBPedia.class);
    private String keyword;
    private final String MAX_HITS = "20";
    private Set<String> noresultList;
    
    public DBPedia() {
        URL url = ClassLoader.getSystemResource(BlackOutConstants.NO_RESULT_FILE);
        System.out.println(url);
        System.out.println(url.toString());
        try {
            noresultList = new HashSet<>(Files.readAllLines(Paths.get(url.getPath()), Charset.defaultCharset()));
        } catch (IOException ex) {
            logger.debug("cannot read noresult list");
        }
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public UndirectedGraph<String, DefaultEdge> lookup(String keyword, TokenEnhancer enhancer) {        
        this.keyword = keyword;
        logger.info("Fetching '{}' from dbpedia...", keyword);
        UndirectedGraph<String, DefaultEdge> tokenTree = null;
        if (checkCache(keyword)) {
            parseResult(keyword, "", enhancer);
        } 
        if(noresultList.contains(keyword)) {
            logger.info("{} has no result", keyword);
            return null;
        }

        URI uri = null;
        try {
            uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("lookup.dbpedia.org")
                    .setPath("/api/search.asmx/KeywordSearch")
                    .setParameter("MaxHits", MAX_HITS)
                    .setParameter("QueryString", keyword)
                    .build();
        } catch (URISyntaxException ex) {
            logger.debug("URISyntaxException");
        }

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(uri);
        httpget.addHeader("Accept", "application/json");
        CloseableHttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
            InputStream is = response.getEntity().getContent();
//            cacheData(keyword, is);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                result.append(line);
            }
            parseResult(keyword, result.toString(), enhancer);
        } catch (IOException ex) {
            ex.printStackTrace();            
        } finally {
            try {
                response.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return tokenTree;
    }

    private boolean checkCache(String keyword) {
        File f = new File(BlackOutConstants.DBPEDIA_CACHE_DIR + keyword + ".json");
        return f.exists();
    }

    private void cacheData(String keyword, String json) {
        try {
            //        File f = new File(BlackOutConstants.DBPEDIA_CACHE_DIR + keyword + ".json");
            Files.write(Paths.get(BlackOutConstants.DBPEDIA_CACHE_DIR + keyword + ".json"), json.getBytes());
        } catch (IOException ex) {
            logger.debug("failed to cache result for \"{}\"", keyword);
        }
    }

    private void addtoNoResult(String keyword) {
        if(noresultList.add(keyword)) {
            URL url = ClassLoader.getSystemResource(BlackOutConstants.NO_RESULT_FILE);
            File f = new File(url.getPath());
            try (FileWriter fileWriter = new FileWriter(f,true)) {
                fileWriter.append(keyword);
            } catch (IOException ex) {
                logger.debug("failed adding \"{}\" to noresult-list", keyword);
            }
        }
    }
    
    public void parseResult(String keyword, String json, TokenEnhancer enhancer) {
        
        if("".equals(json)) {
            File f = new File(BlackOutConstants.DBPEDIA_CACHE_DIR + keyword + ".json");
            try {
                json = FileUtils.readFileToString(f);
            } catch (IOException ex) {
                logger.debug("failed to read cache file for \"{}\"", keyword);
            }
        }
//        System.out.println(json);        
        JSONArray arr = new JSONObject(json).getJSONArray("results");
        if(arr.length() > 0) {
            cacheData(keyword, json);
        } else {
            addtoNoResult(keyword);
        }
        
        for (int i = 0; i < arr.length(); i++) {
            JSONObject res = arr.getJSONObject(i);
            String label = res.getString("label");
//            System.out.println("result " + i + " " + label);
            enhancer.addResult(label);

            JSONArray classes = res.getJSONArray("classes");
//            System.out.println("CLASSES");
            for (int j = 0; j < classes.length(); j++) {
                String cls = classes.getJSONObject(j).getString("label");
                enhancer.addCategory(cls);
//                System.out.println(cls);
            }

            JSONArray cats = res.getJSONArray("categories");
//            System.out.println("CATEGORIES");
            for (int j = 0; j < cats.length(); j++) {
                String cat = cats.getJSONObject(j).getString("label");
                enhancer.addCategory(cat);
//                System.out.println(cat);
            }
        }                
    }

    public static void main(String[] args) throws IOException {
        DBPedia dp = new DBPedia();
        
    }
}
