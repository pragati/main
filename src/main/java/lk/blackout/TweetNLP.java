package lk.blackout;

//import cmu.arktweetnlp.POSTagger;
//import cmu.arktweetnlp.Token;
import cmu.arktweetnlp.Tagger;
import cmu.arktweetnlp.Tagger.TaggedToken;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import lk.blackout.value.TokenVO;
import lk.blackout.value.TwitterVO;
import opennlp.tools.postag.POSModel;
import opennlp.tools.tokenize.TokenizerModel;

/**
 *
 * @author lk
 */
public class TweetNLP {

    private static final String MODEL_TOKEN_EN = "en-token.bin";
    private static final String MODEL_POS_EN = "en-pos-maxent.bin";
    private static final String ARK_MODEL = "/cmu/arktweetnlp/model.20120919";

    private InputStream tokenmodelstream;
    private InputStream posmodelstream;
    private Tagger tagger;
    private TokenizerModel tokenizermodel = null;
    private POSModel posmodel = null;

    public TweetNLP() throws IOException {
//        tokenmodelstream = this.getClass().getClassLoader().getResourceAsStream(MODEL_TOKEN_EN);
//        posmodelstream = this.getClass().getClassLoader().getResourceAsStream(MODEL_POS_EN);
        tagger = new Tagger();
        setupModels();
    }

    private void setupModels() throws IOException {
        try {
//            tokenizermodel = new TokenizerModel(tokenmodelstream);
//            posmodel = new POSModel(posmodelstream);
            tagger.loadModel(ARK_MODEL);

        } finally {
            if (tokenmodelstream != null) {
                try {
                    tokenmodelstream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (posmodelstream != null) {
                try {
                    posmodelstream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void openTokenizer() {

    }

    public void openPOSTagger() {

    }
    
    public List<TaggedToken> arkTagger(String text) {
        return  tagger.tokenizeAndTag(text);
    }

    public List<TwitterVO> posTagger(List<TwitterVO> tweetCollection) {

        for (TwitterVO aTwitterVO : tweetCollection) {
            List<TaggedToken> taggedTokens = arkTagger(aTwitterVO.getTweet());
            List<TokenVO> tokens = new ArrayList<>();
            for (TaggedToken aToken : taggedTokens) {
                TokenVO aTokenVO = new TokenVO();
                aTokenVO.setToken(aToken.token);
                aTokenVO.setTag(aToken.tag);
                tokens.add(aTokenVO);
//                System.out.printf("%s\t%s\n", token.tag, token.token);
            }
            aTwitterVO.setTokenList(tokens);
        }

        return tweetCollection;
    }

    public static void main(String[] args) throws IOException {
//        String tweet = "What was the use of #FIFA giving   .   Luis Suarez    ?   Messi neymar is good month ban only to Manchester United reduce it to 1 month later?";
        String tweet = "ManUtd fans, go to http://www.youarefootball.barclays.co.uk for a chance to win a pair of tickets to see van Gaal's men battle Everton at Old Trafford.";
        TweetNLP inst = new TweetNLP();
//        inst.arkPOSTagger(tweet);
    }

}
