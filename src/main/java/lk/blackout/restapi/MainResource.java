/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */

package lk.blackout.restapi;

import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lk.blackout.business.AsyncTest;
import lk.blackout.business.ClassifyTweets;
import lk.blackout.utils.MsgBuffer;

/**
 *
 * @author lk
 */
@Path("/process")
public class MainResource {
    
    @POST
    @Path("/{twitterId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response startProcess(@PathParam("twitterId") String twitterId) throws IOException { 
        System.out.println("received twitterId:" + twitterId);
        Thread th = new Thread(new ClassifyTweets(twitterId.trim()));
        th.start();
        return Response.ok("").build();
    }
    
    @GET
    @Path("/console")
    @Produces(MediaType.APPLICATION_JSON)
    public Response statusUpdate() { 
        return Response.ok(MsgBuffer.getMsgs()).build();
    }
    
    @GET
    @Path("/clear")
    @Produces(MediaType.APPLICATION_JSON)
    public Response clearStatusUpdate() { 
        MsgBuffer.clearMsgs();
        return Response.ok().build();
    }
    
    @POST
    @Path("/addMsg/{msg}")
    public Response clearStatusUpdate(@PathParam("msg") String msg) { 
        MsgBuffer.addMsg(msg);
        return Response.ok().build();
    }
    
    @GET
    @Path("/test")
    public Response testThreads() { 
        AsyncTest at = new AsyncTest("lk");
        Thread th = new Thread(at);
        String tmp = at.checkTwitter();
        th.start();
        return Response.ok(tmp).build();
    }
    
    
}
