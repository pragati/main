/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lk.blackout.dataEnhancer;

import java.io.IOException;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author lk
 */
public interface TokenEnhancer {
    
    void enhance(String token) throws IOException;
    
    void addResult(String result);
    
    void addCategory(String category);
    
    void removeNode(String category);
    
    void setResultRoot(String result);
    
    String getResutRoot();
    
    UndirectedGraph<String, DefaultEdge> getGraph();
    
    
}
