package lk.blackout.dataEnhancer;

import java.io.IOException;
import lk.blackout.dataEngines.DBPedia;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

/**
 *
 * @author lk
 */
public class DBpediaEnhancer implements TokenEnhancer {

    UndirectedGraph<String, DefaultEdge> tokenGraph;
    private String rootToken;
    private String resultRoot;
    static DBPedia dbpedia;    
    
    public DBpediaEnhancer() {
        this.dbpedia = new DBPedia();
    }
    
    @Override
    public void enhance(String token) throws IOException {
        tokenGraph = new SimpleGraph<>(DefaultEdge.class);
        this.rootToken = token;
        tokenGraph.addVertex(rootToken);
                
        dbpedia.lookup(token, this);
    }
    
    @Override
    public void addResult(String result) {
        this.resultRoot = result;
        if(tokenGraph.addVertex(result)) {
            tokenGraph.addEdge(rootToken, result);
        }
    }
    
    @Override
    public void addCategory(String category) {
        if(tokenGraph.addVertex(category)) {
            tokenGraph.addEdge(resultRoot, category);
        }
    }
    
    @Override
    public void removeNode(String category) {
        tokenGraph.removeVertex(category);
    }
    
    @Override
    public void setResultRoot(String result) {
        this.resultRoot = result;
    }
    
    @Override
    public String getResutRoot() {
        return resultRoot;
    }
    
    @Override
    public UndirectedGraph<String, DefaultEdge> getGraph() {
        return tokenGraph; 
    }
        
    public static void main(String[] args) {
//        DBpediaEnhancer de = new DBpediaEnhancer("test");
    }    
}
