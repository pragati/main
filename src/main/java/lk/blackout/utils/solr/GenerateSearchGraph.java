/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.utils.solr;

import lk.blackout.exceptions.BlastException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;

/**
 *
 * @author lk
 */
public class GenerateSearchGraph {
    final Logger logger = LogManager.getLogger(GenerateSearchGraph.class.getName());
    
    SolrServer solrServer;
    
    public GenerateSearchGraph() {
        SolrUtils su = new SolrUtils("blast-test");
        try {
            solrServer = su.getSolrServer();
        } catch (BlastException bex) {
            logger.error("unable to connect to solr.");
        }
    }
    
    private void addClasses() throws SolrServerException {
        SolrQuery query = new SolrQuery();
        query.setQuery("type:*");
        query.setRows(0);
        query.setFacet(true);
        query.addFacetField("type");
        
        FacetField ff = solrServer.query(query).getFacetField("type");
        
        for (Count cnt : ff.getValues()) {
            System.out.println(cnt.getName());
            System.out.println(cnt.getCount());
        }
    }
    
    public static void main(String[] args) throws SolrServerException {
        GenerateSearchGraph gsg = new GenerateSearchGraph();
        gsg.addClasses();
    }
}
