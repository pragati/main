/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.utils.solr;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import lk.blackout.exceptions.BlastException;
import lk.blackout.utils.BlackOutConstants;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author lk
 */
public class Freebase2Solr {

    final Logger logger = LogManager.getLogger(Freebase2Solr.class.getName());
    SolrServer solrServer;

    public Freebase2Solr() {
        SolrUtils su = new SolrUtils("blast-test");
        try {
            solrServer = su.getSolrServer();
        } catch (BlastException bex) {
            logger.error("unable to connect to solr.");
        }
    }

    public List<FreebaseEntry> parseMQLtoSolrPOJO(String fileName) {
        List<FreebaseEntry> beans = new ArrayList<>();
        String jsonContent = "";
        String type = "";

        if (fileName.contains("football-players")) {
            type = "player";
        } else if (fileName.contains("football-teams")) {
            type = "team";
        } else {
            type = fileName;
        }

        if (!fileName.endsWith(".json")) {
            fileName = fileName + ".json";
        }
        File f = new File(BlackOutConstants.FREEBASE_CACHE_DIR + fileName);
        try {
            jsonContent = FileUtils.readFileToString(f).trim();
        } catch (IOException ex) {
            logger.debug("failed to read file - \"{}\"", fileName);
        }
        
        try {
            JSONArray resultsArr = new JSONObject(jsonContent).getJSONArray("result");
            for (int i = 0; i < resultsArr.length(); i++) {
                FreebaseEntry fe = new FreebaseEntry();
                JSONObject res = resultsArr.getJSONObject(i);

                try {
                    fe.setName(res.getString("name"));
                } catch (Exception ex) {
//                ex.printStackTrace();
                    System.out.println(fileName);
                    System.out.println(res.getString("mid"));
                    continue;
                }

                fe.setMid(res.getString("mid"));
                fe.setId(res.getString("id"));
                fe.setJson(res.toString());
                fe.setType(type);

                JSONArray aliases = res.getJSONArray("/common/topic/alias");
                List<String> aka = new ArrayList();
                for (int j = 0; j < aliases.length(); j++) {
                    aka.add((String) aliases.get(j));
                }
                fe.setAliases(aka);
                beans.add(fe);
            }
        } catch (JSONException jex) {
            logger.debug("failed to read json data from - \"{}\"", fileName);
        }

        return beans;
    }

    public void addToSolr(String filename) throws SolrServerException, IOException {
        List<FreebaseEntry> beans = parseMQLtoSolrPOJO(filename);

        if (beans != null && beans.size() > 0) {
            solrServer.addBeans(beans);
            solrServer.commit();
        }
        System.out.println("Added " + beans.size() + " " + filename);
    }

    public void addToSolrDir(String dir) throws IOException, SolrServerException {
        Path path = Paths.get(BlackOutConstants.FREEBASE_CACHE_DIR + dir);
        DirectoryStream<Path> stream = Files.newDirectoryStream(path);
        int count = 0;
        for (Path aFile : stream) {
            String filename = aFile.subpath(5, 7).toString();
            List<FreebaseEntry> beans = parseMQLtoSolrPOJO(filename);

            if (beans != null && beans.size() > 0) {
                solrServer.addBeans(beans);
                count += beans.size();
            }
        }

        System.out.println("Added " + count + " " + dir);

    }

    public static void main(String[] args) throws SolrServerException, IOException {
        Freebase2Solr f2s = new Freebase2Solr();
        f2s.addToSolr("position");
        f2s.addToSolr("stadium");
        f2s.addToSolr("organization");
        f2s.addToSolr("league");
        f2s.addToSolrDir("football-players");
        f2s.addToSolrDir("football-players-team");
        f2s.addToSolrDir("football-teams");
        f2s.solrServer.commit();
        f2s.solrServer.optimize();

    }

}
