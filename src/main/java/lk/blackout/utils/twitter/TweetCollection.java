/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.utils.twitter;

import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author lk
 */
public class TweetCollection {

    
    public static final String JSON_IDENTIFIER_TWEETS = "tweets";
    public static final String JSON_IDENTIFIER_COUNT = "count";

    private ConcurrentSkipListMap<Integer, String> tweets = new ConcurrentSkipListMap<>();
    private ConcurrentSkipListMap<Integer, String> tweetTags = new ConcurrentSkipListMap<>();
    private int nextfreeOffset = 0;
    private int count = 0;

    public TweetCollection() {
    }

    public TweetCollection(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        loadTweets(obj.getJSONArray(JSON_IDENTIFIER_TWEETS));
    }

    private void loadTweets(JSONArray arr) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    protected int getNextCount() {
        return nextfreeOffset;
    }

    public void addTweet(int offset, String tweet) {
        String striped = StringUtils.stripStart(tweet, null);
        int i = tweet.length() - striped.length();
        tweets.put(count, StringUtils.stripEnd(striped.intern(), null));
        count++;
    }

    public synchronized void addTweet(String... inchunks) {
        for (String chunk : inchunks) {
            addTweet(count, chunk);
        }
    }

    public int getLastOffset() {
        if (tweets.isEmpty()) {
            return 0;
        } else {
            return tweets.lastKey();
        }
    }

    public NavigableMap<Integer, String> getTweets() {
//		return chunks.clone();
        return tweets;
    }

    public Map.Entry<Integer, String> getTweet(int offset) {
        return getTweets().floorEntry(offset);
    }

    public void removeAllTweets() {
        tweets = new ConcurrentSkipListMap<>();
    }

    public void removeTweet(int offset) {
        if (tweets.get(offset) != null) {
            tweets.remove(offset);
        } else {
            if (tweets.lowerKey(offset) != null) {
                tweets.remove(tweets.lowerKey(offset));
            }
        }
    }

    public boolean removeTweetExact(int offset) {
        if (tweets.get(offset) != null) {
            tweets.remove(offset);
            return true;
        } else {
            return false;
        }
    }

    public String toJSON() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put(JSON_IDENTIFIER_COUNT, count);
        obj.put(JSON_IDENTIFIER_TWEETS, tweetsToJson());
        return obj.toString();
    }

    private JSONObject tweetsToJson() throws JSONException {
        JSONObject obj = new JSONObject();

        Iterator<Map.Entry<Integer, String>> it = tweets.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, String> entry = it.next();
            obj.put(entry.getKey().toString(), entry.getValue());
        }

        return obj;
    }
}
