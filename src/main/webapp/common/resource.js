/* 
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */

'use strict';

angular.module('blackOut.resources', ['ngResource'])

.factory('mainResource', ['$resource', 'blackOutConfig', function($resource, blackOutConfig) {
	return $resource(blackOutConfig.restUrl + 'process/:twitterId', {twitterId: '@twitterId'}, {
		startProcess: {
			method: 'POST',
			isArray: false
		},
        statusUpdate: {
			method: 'GET',
			isArray: false,
			url: blackOutConfig.restUrl + 'process/console'
        }
	});
}]);