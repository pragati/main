/* 
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */

'use strict';

angular.module('blackOut.config', [])

.value('version', '0.29.a')

.constant('blackOutConfig', {
	restUrl: 'http://localhost:8080/BlackOut/api/',
	tags: {
		"N": "NOUN",
		"V": "VERB",
		"A": "ADJECTIVE",
		"R": "ADVERB",
		"O": "PRONOUN",
		"&": "CONJUNCTION",
		"D": "DETERMINER",
		"P": "PREPOSITION",
		"^": "NOUN PROPER",
		"E": "EMOTICON",
		"#": "HASHTAG",
		"!": "INTERJECTION",
		",": "PUNCTUATION",
		"tilde": "MARKER DISCOURSE",
		"$": "NUMBER",
		"@": "MENTION",
		"G": "ABBREVIATION",
		"L": "VERBAL NOMINAL",
		"M": "PROPER NOUN VERBAL",
		"S": "POSSESSIVE NOMINAL",
		"T": "VERB PARTICLE",
		"U": "URL",
		"X": "EXISTENTIAL",
		"Y": "VERBAL EXISTENTIAL",
		"Z": "PROPER NOUN POSSESSIVE",
	}
})

.config(function($sceProvider) {
 	$sceProvider.enabled(false);
 })

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.otherwise({redirectTo: '/main'});
}]);