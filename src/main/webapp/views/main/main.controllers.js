/* 
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */

'use strict';

angular.module('main.controllers', ['blackOut.resources', 'ui.bootstrap', 'ngSanitize'])

.controller('MainController', ['$scope', '$interval', '$http', '$location', 'mainResource', '$sce', 'blackOutConfig', '$modal', function($scope, $interval, $http, $location, mainResource, $sce, blackOutConfig, $modal) {

 	$scope.twitterId = '';
 	$scope.consoleMsgs = [];
 	$scope.results = [];    
 	$scope.roptions = {
 		showUsefulOnly: true,
 		expandAll: false,
 		oneAtTime: false
 	};
 	var stopPolling;

 	$scope.$watch('roptions', function(newopts, oldopts) {
 		if(newopts.expandAll === true && oldopts.expandAll === false) {
 			$scope.roptions.oneAtTime = false;
 		}
 		if(newopts.oneAtTime === true && oldopts.oneAtTime === false) {
 			$scope.roptions.expandAll = false;
 		}
 	}, true);

	$scope.status = {};

    $scope.testing = function() {
    	$scope.removeSearchBar();
    	$scope.$broadcast('changeHeader');

		var aObj = angular.fromJson('{"tweet":"On Demand: Jamie Carragher and Gary Neville answer your questions on @SkySportsMNF, available now #skyfootball","tweetId":542284375803830273,"tokens":[{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"On","tag":"P"},{"rank":0.16285559292055937,"domainScore":0.408227,"node":"/m/05b3n5x","pathWeight":16,"fbScore":142.957001,"token":"Demand","tag":"N"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":":","tag":","},{"rank":0.8253480888367386,"domainScore":28.417458,"node":"/m/09tyl3","pathWeight":16,"fbScore":42.710663,"token":"Jamie","tag":"^"},{"rank":1.16,"domainScore":29.406942,"node":"/m/03z2p0","pathWeight":16,"fbScore":29.406942,"token":"Carragher","tag":"^"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"and","tag":"&"},{"rank":0.7144848840561152,"domainScore":34.89539,"node":"/m/0pl5s3f","pathWeight":16,"fbScore":62.932987,"token":"Gary","tag":"^"},{"rank":0.9307224180274534,"domainScore":37.351398,"node":"/m/03ryy9","pathWeight":16,"fbScore":48.462841,"token":"Neville","tag":"^"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"answer","tag":"V"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"your","tag":"D"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"questions","tag":"N"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"on","tag":"P"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"@SkySportsMNF","tag":"@"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":",","tag":","},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"available","tag":"A"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"now","tag":"R"},{"rank":0,"domainScore":0,"pathWeight":0,"fbScore":0,"token":"#skyfootball","tag":"#"}]}');
		var aTweet = aObj.tweet;
		var usefulTokens = _.filter(aObj.tokens, 'node');

		var textReplaceFunc = function(tokenObj) {
			var tooltipText = 'Tagged as ' + blackOutConfig.tags[tokenObj.tag];
			var token = '<span class="token" title="' + tooltipText + '">';
			token += tokenObj.token + '</span>';
			return token;
		};

		for (var i = 0; i < usefulTokens.length; i++) {
			// console.log(usefulTokens[i]);
			aTweet = aTweet.replace(usefulTokens[i].token, textReplaceFunc(usefulTokens[i]));
		};

		aObj.tweet = $sce.trustAsHtml(aTweet);
		for (var i = aObj.tokens.length - 1; i >= 0; i--) {
			aObj.tokens[i].tag = blackOutConfig.tags[aObj.tokens[i].tag];
		};
		aObj.isOpen = false;
    	$scope.results.push(aObj);
    };

 	$scope.startProcess = function() {
    	$scope.removeSearchBar();
    	$scope.$broadcast('changeHeader');

        mainResource.startProcess({twitterId: $scope.twitterId});

 		if ( angular.isDefined(stopPolling) ) return;

 		stopPolling = $interval(function() {
 			$http({
 				url: 'http://localhost:8080/BlackOut/api/process/console',
 				method: "GET",
 			}).success(function (data, status, headers, config) {
				angular.forEach(data.msgs, function(aMsg) {
 					if(aMsg == 'EOM'){
 						$scope.stopPollingFunc();
 					} else {
 						$scope.consoleMsgs.push(aMsg);
 					}
				});


				console.log(data.results);

 				angular.forEach(data.results, function(aResult) {
					
					aResult.tokens = angular.copy(aResult.tokens);

					var aTweet = aResult.tweet;
					var usefulTokens = _.filter(aResult.tokens, 'matched');

					var textReplaceFunc = function(tokenObj) {
						var tooltipText = 'Tagged as ' + blackOutConfig.tags[tokenObj.tag];
						var token = '<span class="token" title="' + tooltipText + '">';
						token += tokenObj.token + '</span>';
						return token;
					};

					for (var i = 0; i < usefulTokens.length; i++) {
						// console.log(usefulTokens[i]);
						aTweet = aTweet.replace(usefulTokens[i].token, textReplaceFunc(usefulTokens[i]));
					};

					aResult.tweet = $sce.trustAsHtml(aTweet);
					for (var i = aResult.tokens.length - 1; i >= 0; i--) {
						aResult.tokens[i].tag = blackOutConfig.tags[aResult.tokens[i].tag];
						aResult.tokens[i].matchedPart = $sce.trustAsHtml(aResult.tokens[i].matchedPart);
					};
					aResult.isOpen = false;
					$scope.results.push(aResult); 							
 				});

 				if(data.results.length > 0) {
 					$scope.consoleMsgs.push("Fetched " + $scope.results.length + " results");
 				}

 			}).error(function (data, status, headers, config) {
		        $scope.stopPollingFunc();
            });	
 		}, 2500);
		// $location.path('/result/' + $scope.twitterId);
	};

	$scope.stopPollingFunc = function() {
		if (angular.isDefined(stopPolling)) {
			$interval.cancel(stopPolling);
			stopPolling = undefined;
		}		
	};

	$scope.$on('$destroy', function() {
		// Make sure that the interval is destroyed too
		$scope.stopPollingFunc();
	});

	$scope.openInfoModal = function() {
		$modal.open({
			templateUrl: 'myModalContent.html',
			controller: 'ModalInstanceCtrl',
			// size: size,
			windowClass: 'info-modal'
	    });		
	}


}])

.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', function($scope, $modalInstance) {
	$scope.ok = function () {
		$modalInstance.dismiss();
	};	
}]);