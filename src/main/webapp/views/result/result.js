/* 
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */

'use strict';

angular.module('blackOut.result', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/result/:twitterId', {
    templateUrl: 'views/result/result.html',
    controller: 'ResultCtrl'
  });
}])

.controller('ResultCtrl', [function() {

}]);